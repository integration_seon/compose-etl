# Compose Etl
Basicamente a arquitetura que obtém os dados é composta de:
- Um banco de dados Postgresql
- Um container especializado em preencher o banco seguindo ontologia SRO
- Banco de dado em Mongo
- RabbitMQ
- Um webservice em python (Realtime)
- Um container com Metabase (interface de acesso e análise de dados)
- Um job em python (Jira_hub)

Para rodar o script compose.sh utilize os seguintes comandos:

    chmod +x compose.sh
    source compose.sh

Para apagar da máquina todos os containers criados utilize os seguintes comandos:
   
    chmod +x clean.sh
    source clean.sh 
