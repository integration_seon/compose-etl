# Para todos os containers criados
docker container stop \
    metabase \
    realtime \
    postgres \
    jira_hub \
    create-db \
    rabbit mongodb

# Remove todos os containers criados
docker container rm \
    metabase \
    realtime \
    postgres \
    jira_hub \
    create-db \
    rabbit \
    mongodb 

# Remove todas as imagens baixadas de containers
docker rmi \
    00kl/create_db-wize:latest \
    00kl/metabase-wize:latest \
    00kl/realtime-wize:latest \
    00kl/jira_hub-wize:latest \
    rabbitmq:3-management \
    postgres:latest \
    mongo:4.4.6-bionic 

# Remove network criada para comunicação entre os containers
docker network rm wize-network
